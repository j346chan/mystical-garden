class fieldAgent {
  
    constructor(animation, x, y, size, speed, tintColour) {
      
      this.animation = animation;
      this.x = x;
      this.y = y;
      this.size = size;
      this.w = this.animation[0].width;
      //this.h = this.animation[0].height;
      this.len = this.animation.length;
      this.speed = speed;
      this.index = 0;
      this.tintColour = tintColour;
      this.fade = 0;
  
      //imageMode(CENTER);
    }
    
    // functions
    
    
    show() {

        push();
        let index = floor(this.index) % this.len;
        //image(this.animation[index], 0, 0, this.w, this.h);
        tint(this.tintColour);
        tint(255, this.fade);
        image(this.animation[index], this.x, this.y, this.size, this.size / 1.75);
        
        if (this.fade < 255) {
          this.fade += 5;
        }
      
        if ((index > 30) && (this.fade > 0)) {
          this.fade -= 10;
        }

        if (index == this.animation.length - 1) {
            this.x = random(-50, windowWidth - 50);
            this.y = random(windowHeight / 4 * 3, windowHeight - 25);
            this.size = random(50, 100);
            this.speed = random(0.1, 0.4);
        }

        pop();
    }
    
    animate() {
      this.index += this.speed;
      
    }
    
}

  

