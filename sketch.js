// BodyPix 2.0 Set Up by following Coding Train Live 184: UNet and BodyPix with Ml5.Js
// Link: www.youtube.com/watch?v=jKHgVdyC55M&t=8142s

// variables for BodyPix
let video;
let bodyPix;
let segmentedImage;

// additional parameters for posenet
let poseNet;
let pose;
let skeleton;

// store background image
let tempbackground;

// grass patch image
let grass;

// store images for GIF
let fieldGIF = [];
let flowerGIF = [];

// store field flower agents
let fieldFlowers = [];
let bodyFlowers = [];


// track stillness of full body
let previousKeyPoints;
let isStill = false;
let stillTimer = 0;
let firstpose = true;

// track and create plant sihouette
let plantSilhouette = [];
let sculptCreated = false;
let frameCreated;


function preload() {

  // store background
    tempbackground = loadImage('assets/background.jpg');

    // store images for GIF
    for (i = 0; i < 50; i++) {
      path = 'assets/fieldflower/rose' + str(i) + '.png';
      img = loadImage(path);
      fieldGIF.push(img);
    }

    for (i = 0; i < 46; i++) {
      path = 'assets/bodyflower/flower' + str(i) + '.png';
      img = loadImage(path);
      flowerGIF.push(img);
    }
  
  // store leaf for body silhouette
  grass = loadImage('assets/newgrass.png');

}



function setup() {

    // set up canvas
    createCanvas(windowWidth, windowHeight);

    let tintColour = ['mediumorchid', 'darkgoldenrod', 'thistle', 'mistyrose', 'coral'];

    var idxCount = 0;

    // create Agents
    for (i = 0; i < 5; i++) {

      for (j = 0; j < tintColour.length; j++) {

        //let tintColour = ['darkgoldenrod'];

        var x = random(-50, width - 50);
        var y = random(height / 4 * 3, height - 25);
        var size = random(75, 125);
        fieldFlowers[idxCount] = new fieldAgent(fieldGIF, x, y, size, random(0.1, 0.4), tintColour[j]);
        idxCount += 1;

      }

    }

    // set up video and BodyPix
    video = createCapture(VIDEO, videoReady);
    video.size(windowWidth, windowHeight);
    video.hide();
    segmentedImage = createImage(windowWidth, windowHeight);

    // poseNet
    poseNet = ml5.poseNet(video, modelReady);
    poseNet.on('pose', gotPoses);


}


// poseNet function
function gotPoses(poses) {
  console.log(poses); 
  if (poses.length > 0) {
    pose = poses[0].pose;
    skeleton = poses[0].skeleton;
  }
}


// functions for BodyPix:
//  modelReady, videoReady, gotResult, 
function modelReady() {
  console.log('model ready');
  bodyPix.segment(gotResult);
}


function videoReady() {
  bodyPix = ml5.bodyPix(video, modelReady);
  console.log('video ready');
}


function gotResult(error, result) {
  console.log(result);
  // if there's an error return it
  if (error) {
    console.error(error);
    return;
  }
  // log your result
  // console.log(result);
  // image(result.image, 0, 0, width, height);
  segmentedImage = result.maskPerson;

  bodyPix.segment(gotResult);
}



function draw() {

    background(50);
    //image(tempbackground, 0, 0, 1730, 1080);
    //image(tempbackground, 0, 0);

    push();
    tint(245, 245, 245, 200);
    // adjust background based on ratio
    image(tempbackground, 0, 0, width, height);
    pop();


    
    // add the field flowers behind the person
    for (let flower of fieldFlowers) {
      flower.show();
      flower.animate();
    }
    


    
    // reflect video to mirror audience
    translate(windowWidth, 0);
    scale(-1, 1);
  
    push();
    // create "faded silhouette" effect
    tint(255, 0, 0, 100)
    // display video silhouette (bodyPix)
    image(segmentedImage, 0, 0, width, height);
    pop();

    
    if (pose) {

      if (firstpose == true) {
        // just store the point in the previouskeyinput
        previousKeyPoints = pose.keypoints;
        
        firstpose = false;
      }
      
      else {
        // check if person is standing still
        var isMoving = checkMovement(pose);
        
        if (isMoving == false) {
          // not moving
          stillTimer += 1;
        }
        else {
          stillTimer = 0;
        }
        
        console.log('stillTimer value = ' + stillTimer);
        
        // check how long person has been standing still
        if (stillTimer >= 20) {
          isStill = true;
          console.log('person is still for at least 5 seconds');
          
          if (sculptCreated == false) {
            
            frameCreated = frameCount;
            let rightintx;
            let rightinty;
            let leftintx;
            let leftinty;
            let midintx;
            let midinty;
            
            // create graphics
            // organize silhouette array by body parts, bottom up
            
            // rightAnkle
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[16].position.x, pose.keypoints[16].position.y, 125, frameCreated, frameCreated + (10 * 60) + 70));
            // leftAnkle
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[15].position.x, pose.keypoints[15].position.y, 125, frameCreated, frameCreated + (10 * 60) + 70));
            
            // between knees and ankles
            // right
            rightintx = (pose.keypoints[16].position.x + pose.keypoints[14].position.x) / 2;
            rightinty = (pose.keypoints[16].position.y + pose.keypoints[14].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, rightintx, rightinty, 125, frameCreated + 5, frameCreated + (10 * 60) + 60));
            // left
            leftintx = (pose.keypoints[15].position.x + pose.keypoints[13].position.x) / 2;
            leftinty = (pose.keypoints[15].position.y + pose.keypoints[13].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, leftintx, leftinty, 125, frameCreated + 5, frameCreated + (10 * 60) + 60));
            
            // rightKnee
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[14].position.x, pose.keypoints[14].position.y, 150, frameCreated + 10, frameCreated + (10 * 60) + 50));
            // leftKnee
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[13].position.x, pose.keypoints[13].position.y, 150, frameCreated + 10, frameCreated + (10 * 60) + 50));
            
            // between knees and hips
            // right
            rightintx = (pose.keypoints[14].position.x + pose.keypoints[12].position.x) / 2;
            rightinty = (pose.keypoints[14].position.y + pose.keypoints[12].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, rightintx, rightinty, 175, frameCreated + 15, frameCreated + (10 * 60) + 40));
            // left
            leftintx = (pose.keypoints[13].position.x + pose.keypoints[11].position.x) / 2;
            leftinty = (pose.keypoints[13].position.y + pose.keypoints[11].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, leftintx, leftinty, 175, frameCreated + 15, frameCreated + (10 * 60) + 40));
            
            // rightHip
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[12].position.x, pose.keypoints[12].position.y, 200, frameCreated + 20, frameCreated + (10 * 60) + 30));
            // leftHip
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[11].position.x, pose.keypoints[11].position.y, 200, frameCreated + 20, frameCreated + (10 * 60) + 30));
            // middle of hip
            midintx = (pose.keypoints[12].position.x + pose.keypoints[11].position.x) / 2;
            midinty = (pose.keypoints[12].position.y + pose.keypoints[11].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, midintx, midinty, 200, frameCreated + 20, frameCreated + (10 * 60) + 30));
            
            // between hips and shoulder
            // right
            rightintx = (pose.keypoints[12].position.x + pose.keypoints[6].position.x) / 2;
            rightinty = (pose.keypoints[12].position.y + pose.keypoints[6].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, rightintx, rightinty, 225, frameCreated + 25, frameCreated + (10 * 60) + 20));
            // left
            leftintx = (pose.keypoints[11].position.x + pose.keypoints[5].position.x) / 2;
            leftinty = (pose.keypoints[11].position.y + pose.keypoints[5].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, leftintx, leftinty, 225, frameCreated + 25, frameCreated + (10 * 60) + 20));
            // middle of hips and shoulder
            midintx = (rightintx + leftintx) / 2;
            midinty = (rightinty + leftinty) / 2;
            plantSilhouette.push(new bodyAgent(grass, leftintx, leftinty, 225, frameCreated + 25, frameCreated + (10 * 60) + 20));
            
            // rightShoulder
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[6].position.x, pose.keypoints[6].position.y, 200, frameCreated + 30, frameCreated + (10 * 60) + 10));
            // leftShoulder
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[5].position.x, pose.keypoints[5].position.y, 200, frameCreated + 30, frameCreated + (10 * 60) + 10));
            // middle shoulder
            midintx = (pose.keypoints[6].position.x + pose.keypoints[5].position.x) / 2;
            midinty = (pose.keypoints[6].position.y + pose.keypoints[5].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, midintx, midinty, 200, frameCreated + 30, frameCreated + (10 * 60) + 10));
            // nose / face
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[0].position.x, pose.keypoints[0].position.y, 150, frameCreated + 35, frameCreated + (10 * 60)));
            
            // between shoulder and elbow
            // right
            rightintx = (pose.keypoints[6].position.x + pose.keypoints[8].position.x) / 2;
            rightinty = (pose.keypoints[6].position.y + pose.keypoints[8].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, rightintx, rightinty, 125, frameCreated + 35, frameCreated + (10 * 60) + 20));
            // left
            leftintx = (pose.keypoints[5].position.x + pose.keypoints[7].position.x) / 2;
            leftinty = (pose.keypoints[5].position.y + pose.keypoints[7].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, leftintx, leftinty, 125, frameCreated + 35, frameCreated + (10 * 60) + 20));
            
            // rightElbow
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[8].position.x, pose.keypoints[8].position.y, 125, frameCreated + 40, frameCreated + (10 * 60) + 30));
            // leftElbow
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[7].position.x, pose.keypoints[7].position.y, 125, frameCreated + 40, frameCreated + (10 * 60) + 30));
            
            // between elbows and wrists
            // right
            rightintx = (pose.keypoints[8].position.x + pose.keypoints[10].position.x) / 2;
            rightinty = (pose.keypoints[8].position.y + pose.keypoints[10].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, rightintx, rightinty, 125, frameCreated + 45, frameCreated + (10 * 60) + 40));
            // left
            leftintx = (pose.keypoints[7].position.x + pose.keypoints[9].position.x) / 2;
            leftinty = (pose.keypoints[7].position.y + pose.keypoints[9].position.y) / 2;
            plantSilhouette.push(new bodyAgent(grass, leftintx, leftinty, 125, frameCreated + 45, frameCreated + (10 * 60) + 40));
            
            // rightWrist
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[10].position.x, pose.keypoints[10].position.y, 125, frameCreated + 50, frameCreated + (10 * 60) + 50));
            // leftWrist
            plantSilhouette.push(new bodyAgent(grass, pose.keypoints[9].position.x, pose.keypoints[9].position.y, 125, frameCreated + 50, frameCreated + (10 * 60) + 50));
            
            
            
            sculptCreated = true;
          }
          
          
        }
        else {
          // moved, not still
          isStill = false;
        }
        
        // update previouskeypoints
        previousKeyPoints = pose.keypoints;
      }
      
      
    }
  
    // draw silhouette agent
    for (let patch of plantSilhouette) {
      patch.show();
    }
  
    if (frameCount > (frameCreated + (10 * 60) + 100)) {
      sculptCreated = false;
      plantSilhouette.pop();
    }



  }


function checkMovement(curpose) {
    let moving = false;

    for (let i = 0; i < curpose.keypoints.length; i++) {
      
      if (moving == true) {
        break;
      }
      // ignore points with low confidence
      if (curpose.score > 0.2) {
        // find the difference
        let dx = curpose.keypoints[i].position.x - previousKeyPoints[i].position.x;
        let dy = curpose.keypoints[i].position.y - previousKeyPoints[i].position.y;
        if ((dx >= -20) && (dx <= 20)) {
          if ((dy >= -20) && (dy <= 20)) {
            if (moving == false) {
              moving = false;
            }
            else {
              moving = true;
            }
          }
          else {
            moving = true;
          }
        }
        else {
          moving = true;
        }
      }

      // don't compare with previous points if not enough confidence
    }

    return moving;
  }



