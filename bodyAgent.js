class bodyAgent {
  
    constructor(img, x, y, size, beginFade, seeUntil) {
      
      this.img = img;
      this.x = x - 80;
      this.y = y - 80;
      this.size = size;
      this.w = this.img.width;
      this.fade = 0;
      this.beginFade = beginFade;
      this.seeUntil = seeUntil;
  
      //imageMode(CENTER);
    }
    
    // functions
    
    
    show() {
      
      if (frameCount >= this.beginFade) {

        push();
      
        tint(255, this.fade);
        image(this.img, this.x, this.y, this.size, this.size);
        
        if (this.fade < 255) {
          this.fade++;
        }
      
        if (frameCount > this.seeUntil) {
          this.fade -= 5;
        }
        
      
        pop();
        
      }
      
    }
    

    
}


